/**
 * Copyright 2009 Washington University
 */
package org.nrg;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class Utils {
	private Utils() {}
	
	public static int compare(int a, int b) {
		if (a < b) {
			return -1;
		} else if (a > b) {
			return 1;
		} else {
			return 0;
		}
	}
	
}
