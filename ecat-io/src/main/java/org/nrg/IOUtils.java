/**
 * Copyright 2009 Washington University
 */
package org.nrg;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class IOUtils {
	private IOUtils() {}
	
	private static final int BUF_SIZE = 512;
	
	/**
	 * Copies data from in to out
	 * @param out destination OutputStream
	 * @param in source InputStream
	 * @param nbytes number of bytes to copy
	 * @throws IOException
	 */
	public static void copy(final OutputStream out, final InputStream in, final int nbytes)
	throws IOException {
		final byte[] buf = new byte[BUF_SIZE];
		for (int remaining = nbytes; remaining > 0; ) {
			final int chunk = Math.min(BUF_SIZE, remaining);
			for (int buf_i = 0; buf_i < chunk; ) {
				int nread = in.read(buf, buf_i, chunk - buf_i);
				if (-1 == nread) {
					throw new EOFException();
				} else {
					buf_i += nread;
				}
			}
			out.write(buf, 0, chunk);
			remaining -= chunk;
		}
	}
	
	
	/**
	 * Copies all data from in to out
	 * @param out destination OutputStream
	 * @param in source InputStream
	 * @param pl ProgressListener for monitoring the copy
	 * @throws IOException
	 */
	public static void copy(final OutputStream out, final InputStream in, final ProgressListener pl)
	throws IOException {
		final byte[] buf = new byte[BUF_SIZE];
		while (true) {
			final int nread = in.read(buf);
			if (-1 == nread) {
				return;
			} else {
				out.write(buf, 0, nread);
				if (null != pl) { pl.incrementProgress(nread); }
			}
		}
	}
	
	/**
	 * Copies all data from in to out
	 * @param out destination OutputStream
	 * @param in source InputStream
	 * @throws IOException
	 */
	public static void copy(final OutputStream out, final InputStream in)
	throws IOException {
		copy(out, in, null);
	}
}
