/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface ProgressListener {
	void setMessage(String message);
	void incrementProgress(int increment);
	void incrementTaskSize(int increment);
}
