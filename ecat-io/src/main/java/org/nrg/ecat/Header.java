/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat;

import java.util.Collection;
import java.util.SortedMap;

import org.nrg.ecat.var.Variable;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface Header {
	Type getType();
	Object getValue(Variable variable);
	SortedMap getVariableValues();
	Collection getVariables();
	
	public final static class Type implements Comparable {
		private final String name;
		private final Integer code;
		
		private Type(final String name, final int code) {
			this.name = name;
			this.code = new Integer(code);
		}
		
		public String getName() { return name; }
		
		/*
		 * (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(final Object o) {
			return code.compareTo(((Type)o).code);
		}
		
		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return super.toString() + " " + name;
		}
	}
	
	public final static Type MAIN = new Type("Main", 0);
}
