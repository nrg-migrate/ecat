/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.nrg.ecat.var.CharacterVariable;
import org.nrg.ecat.var.DateTimeVariable;
import org.nrg.ecat.var.RealListVariable;
import org.nrg.ecat.var.RealVariable;
import org.nrg.ecat.var.ShortListVariable;
import org.nrg.ecat.var.ShortVariable;
import org.nrg.ecat.var.Variable;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class MainHeader implements Header {
	private static final String ECAT_MAGIC = "MATRIX7";
	
	private final SortedMap values;
	
	public MainHeader(final InputStream in) throws IOException {
		final SortedMap m = new TreeMap();
		final Iterator vi = allVariables.iterator();
		
		// First variable is (or should be) ECAT magic
		final Variable magicv = (Variable)vi.next();
		assert MAGIC_NUMBER == magicv;
		final String magic = (String)magicv.readValue(in);
		if (!magic.startsWith(ECAT_MAGIC)) {
			throw new IOException("Data not in ECAT format");
		}
		
		while (vi.hasNext()) {
			final Variable v = (Variable)vi.next();
			m.put(v, v.readValue(in));
		}
		values = Collections.unmodifiableSortedMap(m);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.Header#getType()
	 */
	public Type getType() { return MAIN; }

	/* (non-Javadoc)
	 * @see org.nrg.ecat.Header#getValue(org.nrg.ecat.var.Variable)
	 */
	public Object getValue(Variable variable) {
		return values.get(variable);
	}

	/* (non-Javadoc)
	 * @see org.nrg.ecat.Header#getVariableValues()
	 */
	public SortedMap getVariableValues() {
		return values;
	}

	/* (non-Javadoc)
	 * @see org.nrg.ecat.Header#getVariables()
	 */
	public Collection getVariables() {
		return values.keySet();
	}
	
	public String toString() {
		return values.toString();
	}

	// main header
	public static final Variable MAGIC_NUMBER = new CharacterVariable(MAIN,"MAGIC_NUMBER",0,14);
	public static final Variable ORIGINAL_FILE_NAME = new CharacterVariable(MAIN,"ORIGINAL_FILE_NAME",14,32);
	public static final Variable SW_VERSION = new ShortVariable(MAIN,"SW_VERSION",46);
	public static final Variable SYSTEM_TYPE = new ShortVariable(MAIN,"SYSTEM_TYPE",48);
	public static final Variable FILE_TYPE = new ShortVariable(MAIN,"FILE_TYPE",50);
	public static final Variable SERIAL_NUMBER = new CharacterVariable(MAIN,"SERIAL_NUMBER",52,10);
	public static final Variable SCAN_START_TIME = new DateTimeVariable(MAIN,"SCAN_START_TIME",62); 
	public static final Variable ISOTOPE_NAME = new CharacterVariable(MAIN,"ISOTOPE_NAME",66,8);
	public static final Variable ISOTOPE_HALFLIFE = new RealVariable(MAIN,"ISOTOPE_HALFLIFE",74);
	public static final Variable RADIOPHARMACEUTICAL = new CharacterVariable(MAIN,"RADIOPHARMACEUTICAL",78,32);
	public static final Variable GANTRY_TILT = new RealVariable(MAIN,"GANTRY_TILT",110);
	public static final Variable GANTRY_ROTATION = new RealVariable(MAIN,"GANTRY_ROTATION",114);
	public static final Variable BED_ELEVATION = new RealVariable(MAIN,"BED_ELEVATION",118);
	public static final Variable INTRINSIC_TILT = new RealVariable(MAIN,"INTRINSIC_TILT",122);
	public static final Variable WOBBLE_SPEED = new ShortVariable(MAIN,"WOBBLE_SPEED",126);
	public static final Variable TRANSM_SOURCE_TYPE = new ShortVariable(MAIN,"TRANSM_SOURCE_TYPE",128);
	public static final Variable DISTANCE_SCANNED = new RealVariable(MAIN,"DISTANCE_SCANNED",130);
	public static final Variable TRANSAXIAL_FOV = new RealVariable(MAIN,"TRANSAXIAL_FOV",134);
	public static final Variable ANGULAR_COMPRESSION = new ShortVariable(MAIN,"ANGULAR_COMPRESSION",138);
	public static final Variable COIN_SAMP_MODE = new ShortVariable(MAIN,"COIN_SAMP_MODE",140);
	public static final Variable AXIAL_SAMP_MODE = new ShortVariable(MAIN,"AXIAL_SAMP_MODE",142);
	public static final Variable ECAT_CALIBRATION_FACTOR = new RealVariable(MAIN,"ECAT_CALIBRATION_FACTOR",144);
	public static final Variable CALIBRATION_UNITS = new ShortVariable(MAIN,"CALIBRATION_UNITS",148);
	public static final Variable CALIBRATION_UNITS_LABEL = new ShortVariable(MAIN,"CALIBRATION_UNITS_LABEL",150);
	public static final Variable COMPRESSION_CODE = new ShortVariable(MAIN,"COMPRESSION_CODE",152);
	public static final Variable STUDY_TYPE = new CharacterVariable(MAIN,"STUDY_TYPE",154,12);
	public static final Variable PATIENT_ID = new CharacterVariable(MAIN,"PATIENT_ID",166,16);
	public static final Variable PATIENT_NAME = new CharacterVariable(MAIN,"PATIENT_NAME",182,32);
	public static final Variable PATIENT_SEX = new CharacterVariable(MAIN,"PATIENT_SEX",214,1);
	public static final Variable PATIENT_DEXTERITY = new CharacterVariable(MAIN,"PATIENT_DEXTERITY",215,1);
	public static final Variable PATIENT_AGE = new RealVariable(MAIN,"PATIENT_AGE",216);
	public static final Variable PATIENT_HEIGHT = new RealVariable(MAIN,"PATIENT_HEIGHT",220);
	public static final Variable PATIENT_WEIGHT = new RealVariable(MAIN,"PATIENT_WEIGHT",224);
	public static final Variable PATIENT_BIRTH_DATE = new DateTimeVariable(MAIN,"PATIENT_BIRTH_DATE",228);	// TODO: format?
	public static final Variable PHYSICIAN_NAME = new CharacterVariable(MAIN,"PHYSICIAN_NAME",232,32);
	public static final Variable OPERATOR_NAME = new CharacterVariable(MAIN,"OPERATOR_NAME",264,32);
	public static final Variable STUDY_DESCRIPTION = new CharacterVariable(MAIN,"STUDY_DESCRIPTION",296,32);
	public static final Variable ACQUISITION_TYPE = new ShortVariable(MAIN,"ACQUISITION_TYPE",328);
	public static final Variable PATIENT_ORIENTATION = new ShortVariable(MAIN,"PATIENT_ORIENTATION",330);
	public static final Variable FACILITY_NAME = new CharacterVariable(MAIN,"FACILITY_NAME",332,20);
	public static final Variable NUM_PLANES = new ShortVariable(MAIN,"NUM_PLANES",352);
	public static final Variable NUM_FRAMES = new ShortVariable(MAIN,"NUM_FRAMES",354);
	public static final Variable NUM_GATES = new ShortVariable(MAIN,"NUM_GATES",356);
	public static final Variable NUM_BED_POS = new ShortVariable(MAIN,"NUM_BED_POS",358);
	public static final Variable INIT_BED_POSITION = new RealVariable(MAIN,"INIT_BED_POSITION",360);
	public static final Variable BED_POSITION = new RealListVariable(MAIN,"BED_POSITION",364,15);
	public static final Variable PLANE_SEPARATION = new RealVariable(MAIN,"PLANE_SEPARATION",424);
	public static final Variable LWR_SCTR_THRES = new ShortVariable(MAIN,"LWR_SCTR_THRES",428);
	public static final Variable LWR_TRUE_THRES = new ShortVariable(MAIN,"LWR_TRUE_THRES",430);
	public static final Variable UPR_TRUE_THRES = new ShortVariable(MAIN,"UPR_TRUE_THRES",432);
	public static final Variable USER_PROCESS_CODE = new CharacterVariable(MAIN,"USER_PROCESS_CODE",434,10);
	public static final Variable ACQUISITION_MODE = new ShortVariable(MAIN,"ACQUISITION_MODE",444);
	public static final Variable BIN_SIZE = new RealVariable(MAIN,"BIN_SIZE",446);
	public static final Variable BRANCHING_FRACTION = new RealVariable(MAIN,"BRANCHING_FRACTION",450);
	public static final Variable DOSE_START_TIME = new DateTimeVariable(MAIN,"DOSE_START_TIME",454);
	public static final Variable DOSAGE = new RealVariable(MAIN,"DOSAGE",458);
	public static final Variable WELL_COUNTER_CORR_FACTOR = new RealVariable(MAIN,"WELL_COUNTER_CORR_FACTOR",462);
	public static final Variable DATA_UNITS = new CharacterVariable(MAIN,"DATA_UNITS",466,32);
	public static final Variable SEPTA_STATE = new ShortVariable(MAIN,"SEPTA_STATE",498);
	public static final Variable MAIN_CTI_FILL = new ShortListVariable(MAIN,"FILL",500,6);

	private static final List allVariables =
		Collections.unmodifiableList(Arrays.asList(new Variable[] {
				MAGIC_NUMBER,
				ORIGINAL_FILE_NAME,
				SW_VERSION,
				SYSTEM_TYPE,
				FILE_TYPE,
				SERIAL_NUMBER,
				SCAN_START_TIME,
				ISOTOPE_NAME,
				ISOTOPE_HALFLIFE,
				RADIOPHARMACEUTICAL,
				GANTRY_TILT,
				GANTRY_ROTATION,
				BED_ELEVATION,
				INTRINSIC_TILT,
				WOBBLE_SPEED,
				TRANSM_SOURCE_TYPE,
				DISTANCE_SCANNED,
				TRANSAXIAL_FOV,
				ANGULAR_COMPRESSION,
				COIN_SAMP_MODE,
				AXIAL_SAMP_MODE,
				ECAT_CALIBRATION_FACTOR,
				CALIBRATION_UNITS,
				CALIBRATION_UNITS_LABEL,
				COMPRESSION_CODE,
				STUDY_TYPE,
				PATIENT_ID,
				PATIENT_NAME,
				PATIENT_SEX,
				PATIENT_DEXTERITY,
				PATIENT_AGE,
				PATIENT_HEIGHT,
				PATIENT_WEIGHT,
				PATIENT_BIRTH_DATE,
				PHYSICIAN_NAME,
				OPERATOR_NAME,
				STUDY_DESCRIPTION,
				ACQUISITION_TYPE,
				PATIENT_ORIENTATION,
				FACILITY_NAME,
				NUM_PLANES,
				NUM_FRAMES,
				NUM_GATES,
				NUM_BED_POS,
				INIT_BED_POSITION,
				BED_POSITION,
				PLANE_SEPARATION,
				LWR_SCTR_THRES,
				LWR_TRUE_THRES,
				UPR_TRUE_THRES,
				USER_PROCESS_CODE,
				ACQUISITION_MODE,
				BIN_SIZE,
				BRANCHING_FRACTION,
				DOSE_START_TIME,
				DOSAGE,
				WELL_COUNTER_CORR_FACTOR,
				DATA_UNITS,
				SEPTA_STATE,
				MAIN_CTI_FILL,
		}));
}
