/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat;

import org.nrg.Utils;


/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public abstract class AbstractHeaderModification implements HeaderModification {
	private final Header.Type type;
	private final int offset;
	
	protected AbstractHeaderModification(final Header.Type type, final int offset) {
		this.type = type;
		this.offset = offset;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.HeaderModification#getHeaderType()
	 */
	public Header.Type getHeaderType() { return type; }
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.HeaderModification#getOffset()
	 */
	public int getOffset() { return offset; }

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		final int headerc = type.compareTo(((HeaderModification)o).getHeaderType());
		return 0 == headerc ? Utils.compare(offset, ((HeaderModification)o).getOffset()) : headerc;
	}
}
