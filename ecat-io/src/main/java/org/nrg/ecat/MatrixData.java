/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;


import org.nrg.IOUtils;
import org.nrg.ProgressListener;
import org.nrg.ecat.var.Variable;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class MatrixData implements Comparable {
	private final static int BLOCK_SIZE = 512;
	private final Map values;
	private final MainHeader mainHeader;
	
	private MatrixData(final InputStream in, final Map requestedVars) throws IOException {
		values = new LinkedHashMap();
		
		mainHeader = new MainHeader(in);

		// First, extract the requested main header variables
		if (requestedVars.containsKey(Header.MAIN)) {
			for (final Iterator i = ((Collection)requestedVars.get(Header.MAIN)).iterator(); i.hasNext(); ) {
				final Variable v = (Variable)i.next();
				values.put(v, mainHeader.getValue(v));
			}
		}
	}
	
	public MatrixData(final InputStream in, final Collection requestedVars)
	throws IOException {
		this(in, arrangeRequestedVars(requestedVars));
	}
	
	public MatrixData(final InputStream in) throws IOException {
		this(in, Collections.EMPTY_MAP);
	}
	
	/**
	 * Implementation for Comparable<MatrixData>
	 * @param o MatrixData to which this should be compared
	 * @return @see {@link java.lang.Comparable#compareTo(Object)}
	 */
	public int compareTo(final MatrixData o) {
		final Date d1 = (Date)getMainHeader().getValue(MainHeader.SCAN_START_TIME);
		final Date d2 = (Date)o.getMainHeader().getValue(MainHeader.SCAN_START_TIME);
		return d1.compareTo(d2);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(final Object o) {
		return compareTo((MatrixData)o);
	}

	
	private static Map arrangeRequestedVars(final Collection requestedVars) {
		final Map m = new HashMap();
		for (final Iterator i = requestedVars.iterator(); i.hasNext(); ) {
			final Variable v = (Variable)i.next();
			if (!m.containsKey(v)) {
				m.put(v, new TreeSet());
			}
			((Collection)m.get(v)).add(v);
		}
		return m;
	}
	
	
	public MainHeader getMainHeader() { return mainHeader; }
	
	
	
	private static void copyOneHeaderWithModifications(final OutputStream out, final InputStream in,
			final List modifications)
	throws IOException {
		// Make any requested modifications, copying data to get to the variables.
		int hpos = 0;
		for (final Iterator i = modifications.iterator(); i.hasNext(); i.remove()) {
			final HeaderModification mod = (HeaderModification)i.next();
			final int offset = mod.getOffset();
			assert offset >= hpos;
			if (offset > hpos) {
				IOUtils.copy(out, in, offset - hpos);
				hpos = offset;
			}
			hpos += mod.modify(out, in);
		}
		
		// Copy the rest of the record.
		if (hpos < BLOCK_SIZE) {
			IOUtils.copy(out, in, BLOCK_SIZE - hpos);
		}
	}
	
	/**
	 * Sorts a single, not-necessarily-ordered Collection of header modifications
	 * into a Map from header types to ordered Lists of modifications.
	 * @param modifications Collection of HeaderModifications
	 * @return Map : Header.Type -> Collection[HeaderModification]
	 */
	private static Map sortModifications(final Collection modifications) {
		final Map m = new HashMap();
		for (final Iterator i = modifications.iterator(); i.hasNext(); ) {
			final HeaderModification hm = (HeaderModification)i.next();
			final Header.Type type = hm.getHeaderType();
			if (!m.containsKey(type)) {
				m.put(type, new ArrayList());
			}
			((Collection)m.get(type)).add(hm);
		}
		for (final Iterator i = m.values().iterator(); i.hasNext(); ) {
			Collections.sort((List)i.next());
		}
		return m;
	}
	
	
	public static void copyWithModifications(final OutputStream out, final InputStream in,
			final Collection modifications, final ProgressListener progress)
			throws IOException {
		final Map m = sortModifications(modifications);
		
		// Record 1: main header
		copyOneHeaderWithModifications(out, in, (List)m.get(Header.MAIN));
		
		// TODO: directory list
		// TODO: matrices
		IOUtils.copy(out, in, progress);
	}
	
	
	public static void copyClearing(final OutputStream out, final InputStream in,
			final Collection variables, final ProgressListener progress) throws IOException {
		final Collection mods = new ArrayList(variables.size());
		for (final Iterator i = variables.iterator(); i.hasNext(); ) {
			mods.add(((Variable)i.next()).createClearModification());
		}
		copyWithModifications(out, in, mods, progress);
	}
	
	
	public static void main(final String[] args) throws IOException {
		for (int i = 0; i < args.length; i++) {
			final FileInputStream in = new FileInputStream(args[i]);
			try {
				final MatrixData m = new MatrixData(in);
				final MainHeader mainHeader = m.getMainHeader();
				System.out.println("Main header:");
				System.out.println(mainHeader);
			} finally {
				in.close();
			}
		}
	}
}
