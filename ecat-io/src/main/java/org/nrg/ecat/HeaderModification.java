/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface HeaderModification extends Comparable {
	Header.Type getHeaderType();
	int getOffset();
	int modify(OutputStream to, InputStream from) throws IOException;
}
