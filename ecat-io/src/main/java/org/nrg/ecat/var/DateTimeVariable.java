/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.nrg.ecat.HeaderModification;
import org.nrg.ecat.Header.Type;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class DateTimeVariable extends AbstractVariable {
	/**
	 * @param type
	 * @param name
	 * @param offset
	 */
	public DateTimeVariable(final Type type, final String name, final int offset) {
		super(type, name, offset);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.AbstractVariable#toString()
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer(super.toString());
		return sb.append(" Integer*4").toString();
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#readValue(java.io.InputStream)
	 */
	public Object readValue(final InputStream in) throws IOException {
		return new Date((0xffffffffl & read4ByteInt(in)) * 1000L);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#createClearModification()
	 */
	public HeaderModification createClearModification() {
		return createClearModification(4);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#createValueModification(java.lang.Object)
	 */
	public HeaderModification createValueModification(final Object value) {
		throw new UnsupportedOperationException();	// TODO: implement
	}
}
