/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.nrg.Utils;
import org.nrg.ecat.AbstractHeaderModification;
import org.nrg.ecat.Header;
import org.nrg.ecat.HeaderModification;
import org.nrg.ecat.Header.Type;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 * 
 * Note: this class has a natural ordering that is inconsistent with equals.
 * 
 * In particular: equals has not been overridden, and compareTo compares only
 * the header type and offset.  Two variables with the same header type and
 * offset will compare as 0, but not be equal (since the test is object
 * identity).  However, two variables with the same header type and offset
 * is pathological.  Don't do that.
 *
 */
public abstract class AbstractVariable implements Variable {
	private Header.Type type;
	private String name;
	private int offset;
	
	protected AbstractVariable(final Header.Type type,
			final String name, final int offset) {
		this.type = type;
		this.name = name;
		this.offset = offset;
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#getHeaderType()
	 */
	public final Type getHeaderType() { return type; }

	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#getName()
	 */
	public final String getName() { return name; }

	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#getOffset()
	 */
	public final int getOffset() { return offset; }
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public final int compareTo(final Object o) {
		final Variable other = (Variable)o;
		final int hc = type.compareTo(other.getHeaderType());
		return 0 == hc ? Utils.compare(offset, other.getOffset()) : hc;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append(offset).append(" ").append(name);
		return sb.toString();
	}
	
	protected static byte[] readFull(final InputStream in, final int size)
	throws IOException {
		final byte[] bytes = new byte[size];
		int n = 0;
		while (n < size) {
			final int nr = in.read(bytes, n, size - n);
			if (nr < 0) {
				throw new EOFException("End of file reached where " + size + " byte field was expected");
			} else {
				assert nr > 0;
				n += nr;
			}
		}
		return bytes;
	}
	
//	public static String showByteArray(final byte[] bytes) {
//		final StringBuffer sb = new StringBuffer("bytes: ");
//		if (0 == bytes.length) {
//			return sb.append("(empty)").toString();
//		} else {
//			sb.append(Integer.toHexString(0xff & bytes[0]));
//		}
//		for (int i = 1; i < bytes.length; i++) {
//			sb.append(", ");
//			sb.append(Integer.toHexString(0xff & bytes[i]));
//		}
//		return sb.toString();
//	}
	
	protected static int read4ByteInt(final InputStream in) throws IOException {
		final byte[] bytes = readFull(in, 4);
		int ib3 = bytes[0] & 0xff;
		int ib2 = bytes[1] & 0xff;
		int ib1 = bytes[2] & 0xff;
		int ib0 = bytes[3] & 0xff;
		return (((((ib3 << 8) | ib2) << 8) | ib1) << 8) | ib0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#createClearModification()
	 */
	public HeaderModification createClearModification(final int size) {
		return new AbstractHeaderModification(getHeaderType(), getOffset()) {		
			public int modify(OutputStream to, InputStream from) throws IOException {
				readFull(from, size);
				to.write(new byte[size]);
				return size;
			}
		};
	}
}
