/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.IOException;
import java.io.InputStream;

import org.nrg.ecat.Header;
import org.nrg.ecat.HeaderModification;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface Variable extends Comparable {
	Header.Type getHeaderType();
	int getOffset();
	String getName();
	Object readValue(InputStream in) throws IOException;
	HeaderModification createClearModification();
	HeaderModification createValueModification(Object value);
}
