/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.nrg.ecat.AbstractHeaderModification;
import org.nrg.ecat.HeaderModification;
import org.nrg.ecat.Header.Type;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class CharacterVariable extends AbstractVariable {
	private static final String CHARSET_NAME = "US-ASCII";
	private final int size;
	
	/**
	 * @param type
	 * @param name
	 * @param offset
	 */
	public CharacterVariable(final Type type, final String name, final int offset, final int size) {
		super(type, name, offset);
		this.size = size;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.AbstractVariable#toString()
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer(super.toString());
		sb.append(" Character*").append(size);
		return sb.toString();
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#readValue(java.io.InputStream)
	 */
	public Object readValue(final InputStream in) throws IOException {
		final byte[] bytes = readFull(in, size);
		int len = 0;
		while (len < size && 0 != bytes[len]) {
			len++;	// ECAT strings are NUL-terminated
		}
		return new String(bytes, 0, len, CHARSET_NAME);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#createClearModification()
	 */
	public HeaderModification createClearModification() {
		return createClearModification(size);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.Variable#createValueModification(java.lang.Object)
	 */
	public HeaderModification createValueModification(final Object value) {
		final byte[] bytes = new byte[size];
		if (null != value) {
			final String v = value.toString();
			System.arraycopy(v.getBytes(), 0, bytes, 0, Math.min(size, v.length()));
		}
		
		return new AbstractHeaderModification(getHeaderType(), getOffset()) {
			public int modify(OutputStream to, InputStream from) throws IOException {
				readFull(from, size);
				to.write(bytes);
				return size;
			}
		};
	}
}
