/**
 * Copyright 2009 Washington University
 */
package org.nrg;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class IOUtilsTest extends TestCase {
	private static final byte[] b1 = new byte[]{0, 1, 2, 3};
	private static final byte[] big = new byte[5124];

	/**
	 * Test method for {@link org.nrg.IOUtils#copy(java.io.OutputStream, java.io.InputStream, int)}.
	 */
	public void testCopyN() throws IOException {
		final ByteArrayInputStream in1 = new ByteArrayInputStream(b1);
		in1.mark(b1.length);
		try {
			final ByteArrayOutputStream out1 = new ByteArrayOutputStream(b1.length + 2);
			try {
				IOUtils.copy(out1, in1, 4);
				assertTrue(Arrays.equals(out1.toByteArray(), b1));
				try {
					IOUtils.copy(out1, in1, 1);
					fail("expected IOException on EOF");
				} catch (IOException ok) {}
			} finally {
				out1.close();
			}
			
			in1.reset();
			final ByteArrayOutputStream out2 = new ByteArrayOutputStream(6);
			try {
				IOUtils.copy(out2, in1, 6);
				fail("expected IOException on EOF");
			} catch (IOException ok) {
			} finally {
				out2.close();
			}
		} finally {
			in1.close();
		}
		
		final ByteArrayInputStream in2 = new ByteArrayInputStream(big);
		in2.mark(big.length);
		try {
			final ByteArrayOutputStream out = new ByteArrayOutputStream(big.length);
			try {
				IOUtils.copy(out, in2, big.length);
				assertTrue(Arrays.equals(big, out.toByteArray()));
			} finally {
				out.close();
			}
		} finally {
			in2.close();
		}
	}
	
	/**
	 * Test method for {@link org.nrg.IOUtils#copy(java.io.OutputStream, java.io.InputStream)}.
	 */
	public void testCopy() throws IOException {
		final InputStream in = new ByteArrayInputStream(big);
		try {
			final ByteArrayOutputStream out = new ByteArrayOutputStream(big.length + 1);
			try {
				IOUtils.copy(out, in);
				assertTrue(Arrays.equals(big, out.toByteArray()));
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
	}
}
