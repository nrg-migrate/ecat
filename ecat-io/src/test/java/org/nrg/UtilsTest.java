/**
 * Copyright 2009 Washington University
 */
package org.nrg;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class UtilsTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.Utils#compare(int, int)}.
	 */
	public void testCompare() {
		final Integer ZERO = new Integer(0);
		final Integer ONE = new Integer(1);
		final Integer MONE = new Integer(-1);
		
		assertEquals(ONE.compareTo(ZERO), Utils.compare(1, 0));
		assertEquals(ZERO.compareTo(ONE), Utils.compare(0, 1));
		assertEquals(MONE.compareTo(ZERO), Utils.compare(-1, 0));
		assertEquals(ZERO.compareTo(MONE), Utils.compare(0, -1));
		assertEquals(ONE.compareTo(MONE), Utils.compare(1, -1));
		assertEquals(MONE.compareTo(ONE), Utils.compare(-1, 1));
		assertEquals(ZERO.compareTo(ZERO), Utils.compare(0, 0));
		assertEquals(ONE.compareTo(ONE), Utils.compare(1, 1));
		assertEquals(MONE.compareTo(MONE), Utils.compare(-1, -1));
	}
}
