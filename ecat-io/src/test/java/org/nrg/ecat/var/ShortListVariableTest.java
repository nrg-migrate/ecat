/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;

import org.nrg.ecat.Header;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ShortListVariableTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.ecat.var.ShortListVariable#ShortListVariable(org.nrg.ecat.Header.Type, java.lang.String, long, int)}.
	 */
	public void testShortListVariable() {
		final Variable sv = new ShortListVariable(Header.MAIN, "TEST_VAR", 32, 4);
		assertEquals(Header.MAIN, sv.getHeaderType());
		assertEquals("TEST_VAR", sv.getName());
		assertTrue(32 == sv.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.ShortListVariable#readValue(java.io.InputStream)}.
	 */
	public void testReadValue() throws IOException {
		final Variable sv = new ShortListVariable(Header.MAIN, "TEST_VAR", 16, 3);
		final InputStream in = new ByteArrayInputStream(new byte[]{1,1,0,16,0,9,0,1});
		try {
			final Collection r = (Collection)sv.readValue(in);
			final Iterator i = r.iterator();
			assertEquals(new Short((short)257), i.next());
			assertEquals(new Short((short)16), i.next());
			assertEquals(new Short((short)9), i.next());
			assertFalse(i.hasNext());
			try {
				sv.readValue(in);
				fail("expected IOException on EOF");
			} catch (IOException ok) {}
		} finally {
			in.close();
		}
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.ShortListVariable#toString()}.
	 */
	public void testToString() {
		final Variable v = new ShortListVariable(Header.MAIN, "V", 30, 4);
		assertEquals("30 V(4) Integer*2", v.toString());
	}
}
