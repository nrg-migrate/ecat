/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.nrg.ecat.Header;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class RealVariableTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.ecat.var.RealVariable#RealVariable(org.nrg.ecat.Header.Type, java.lang.String, long)}.
	 */
	public void testRealVariable() {
		final Variable rv = new RealVariable(Header.MAIN, "TEST_VAR", 32);
		assertEquals(Header.MAIN, rv.getHeaderType());
		assertEquals("TEST_VAR", rv.getName());
		assertTrue(32 == rv.getOffset());
	}

	static byte[] toBytes(final int[] ints) {
		final byte[] bytes = new byte[4*ints.length];
		for (int ii = 0; ii < ints.length; ii++) {
			final int bi = ii * 4;
			bytes[bi + 0] = (byte)((ints[ii] & 0xff000000) >> 24); 
			bytes[bi + 1] = (byte)((ints[ii] & 0x00ff0000) >> 16);
			bytes[bi + 2] = (byte)((ints[ii] & 0x0000ff00) >> 8);
			bytes[bi + 3] = (byte)(ints[ii] & 0x000000ff);
		}
		return bytes;
	}
		
	/**
	 * Test method for {@link org.nrg.ecat.var.RealVariable#readValue(java.io.InputStream)}.
	 */
	public void testReadValue() throws IOException {
		final byte[] bytes = toBytes(new int[] {
				Float.floatToRawIntBits(2.1f),
				Float.floatToRawIntBits(-13.4f),
				Float.floatToRawIntBits(2.2f),
				Float.floatToRawIntBits(0.0f),
		});

		final Variable rv = new RealVariable(Header.MAIN, "TEST_VAR", 32);
		final InputStream in = new ByteArrayInputStream(bytes);
		try {
			assertEquals(new Float(2.1), rv.readValue(in));
			assertEquals(new Float(-13.4), rv.readValue(in));
			assertEquals(new Float(2.2), rv.readValue(in));
			assertEquals(new Float(0.0), rv.readValue(in));
			try {
				rv.readValue(in);
				fail("expected IOException on EOF");
			} catch (IOException ok) {}
		} finally {
			in.close();
		}
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.RealVariable#toString()}.
	 */
	public void testToString() {
		final Variable v = new RealVariable(Header.MAIN, "V", 0);
		assertEquals("0 V Real*4", v.toString());
	}
}
