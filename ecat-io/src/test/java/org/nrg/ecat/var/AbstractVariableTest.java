/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.nrg.ecat.Header;
import org.nrg.ecat.HeaderModification;
import org.nrg.ecat.Header.Type;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class AbstractVariableTest extends TestCase {
	private final class ConcreteVariable extends AbstractVariable {
		public ConcreteVariable(final Type type, final String name, final int offset) {
			super(type, name, offset);
		}
		
		public Object readValue(final InputStream in) {
			throw new UnsupportedOperationException();
		}
		
		public HeaderModification createClearModification() {
			return createClearModification(4);
		}
		
		public HeaderModification createValueModification(final Object value) {
			throw new UnsupportedOperationException();	// TODO: implement
		}
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#AbstractVariable(org.nrg.ecat.Header.Type, java.lang.String, long)}.
	 */
	public void testAbstractVariable() {
		final Variable v = new ConcreteVariable(Header.MAIN, "TEST_VAR", 16);
		assertEquals(Header.MAIN, v.getHeaderType());
		assertEquals("TEST_VAR", v.getName());
		assertTrue(16 == v.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#getHeaderType()}.
	 */
	public void testGetHeaderType() {
		final Variable v = new ConcreteVariable(Header.MAIN, "TEST_VAR", 16);
		assertEquals(Header.MAIN, v.getHeaderType());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#getName()}.
	 */
	public void testGetName() {
		final Variable v = new ConcreteVariable(Header.MAIN, "TEST_VAR", 16);
		assertEquals("TEST_VAR", v.getName());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#getOffset()}.
	 */
	public void testGetOffset() {
		final Variable v = new ConcreteVariable(Header.MAIN, "TEST_VAR", 16);
		assertTrue(16 == v.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#compareTo(java.lang.Object)}.
	 */
	public void testCompareTo() {
		final Integer EQ = new Integer(0);
		final Integer LT = new Integer(-1);
		final Integer GT = new Integer(1);

		final Variable v1 = new ConcreteVariable(Header.MAIN, "V1", 8);
		final Variable v1a = new ConcreteVariable(Header.MAIN, "V1a", 8);
		final Variable v2 = new ConcreteVariable(Header.MAIN, "V2", 12);
		assertEquals(EQ, new Integer(v1.compareTo(v1a)));
		assertEquals(EQ, new Integer(v1a.compareTo(v1)));
		assertEquals(LT, new Integer(v1.compareTo(v2)));
		assertEquals(GT, new Integer(v2.compareTo(v1)));
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#createClearModification(int)}.
	 */
	public void testCreateClearModification() throws IOException {
		final Variable v = new ConcreteVariable(Header.MAIN, "V", 2);
		final HeaderModification hm = v.createClearModification();
		assertEquals(Header.MAIN, hm.getHeaderType());
		assertTrue(2 == hm.getOffset());
		
		final InputStream in = new ByteArrayInputStream(new byte[] {1, 2, 3, 4, 5, 6});
		try {
			final ByteArrayOutputStream out = new ByteArrayOutputStream(4);
			try {
				hm.modify(out, in);
				assertTrue(Arrays.equals(new byte[]{0, 0, 0, 0}, out.toByteArray()));
				
				try {
					hm.modify(out, in);
					fail("expected IOException on EOF");
				} catch (IOException ok) {}
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
		
		final InputStream bad = new ByteArrayInputStream(new byte[]{1, 2, 3});
		try {
			final ByteArrayOutputStream out = new ByteArrayOutputStream(4);
			try {
				hm.modify(out, in);
				fail("expected IOException on EOF");
			} catch (IOException ok) {
			} finally {
				out.close();
			}
		} finally {
			bad.close();
		}
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.AbstractVariable#toString()}.
	 */
	public void testToString() {
		final Variable v1 = new ConcreteVariable(Header.MAIN, "V", 0);
		assertEquals("0 V", v1.toString());
	}
}
