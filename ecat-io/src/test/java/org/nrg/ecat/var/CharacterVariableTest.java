/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.nrg.ecat.Header;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class CharacterVariableTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.ecat.var.CharacterVariable#CharacterVariable(org.nrg.ecat.Header.Type, java.lang.String, long, int)}.
	 */
	public void testCharacterVariable() {
		final Variable cv = new CharacterVariable(Header.MAIN, "TEST_VAR", 32, 16);
		assertEquals(Header.MAIN, cv.getHeaderType());
		assertEquals("TEST_VAR", cv.getName());
		assertTrue(32 == cv.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.CharacterVariable#readValue(java.io.InputStream)}.
	 */
	public void testReadValue() throws IOException {
		final byte[] bytes = {
				't', 'e', 's', 't', 0, 0, 0, 0,
				'a', 'n', 'o', 't', 'h', 'e', 'r', '1'
		};
		final Variable cv = new CharacterVariable(Header.MAIN, "TEST_VAR", 2, 8);
		final InputStream in = new ByteArrayInputStream(bytes);
		try {
			assertEquals("test", cv.readValue(in));
			assertEquals("another1", cv.readValue(in));
			try {
				cv.readValue(in);
			} catch (IOException ok) {}
		} finally {
			in.close();
		}
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.CharacterVariable#toString()}.
	 */
	public void testToString() {
		final Variable cv = new CharacterVariable(Header.MAIN, "V", 18, 32);
		assertEquals("18 V Character*32", cv.toString());
	}
}
