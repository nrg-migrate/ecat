/**
 * Copyright 2009 Washington University
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.nrg.ecat.Header;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ShortVariableTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.ecat.var.ShortVariable#ShortVariable(org.nrg.ecat.Header.Type, java.lang.String, long)}.
	 */
	public void testShortVariable() {
		final Variable sv = new ShortVariable(Header.MAIN, "TEST_VAR", 32);
		assertEquals(Header.MAIN, sv.getHeaderType());
		assertEquals("TEST_VAR", sv.getName());
		assertTrue(32 == sv.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.ShortVariable#readValue(java.io.InputStream)}.
	 */
	public void testReadValue() throws IOException {
		final Variable sv = new ShortVariable(Header.MAIN, "TEST_VAR", 32);
		final InputStream in = new ByteArrayInputStream(new byte[]{0,2,0,0});
		try {
			assertEquals(new Short((short)2), sv.readValue(in));
			assertEquals(new Short((short)0), sv.readValue(in));
			try {
				sv.readValue(in);
				fail("expected IOException on EOF");
			} catch (IOException ok) {}
		} finally {
			in.close();
		}
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.ShortVariable#toString()}.
	 */
	public void testToString() {
		final Variable v = new ShortVariable(Header.MAIN, "V", 6);
		assertEquals("6 V Integer*2", v.toString());
	}
}
