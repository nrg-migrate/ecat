/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit.fn;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.nrg.ecat.edit.ScriptEvaluationException;
import org.nrg.ecat.edit.ScriptFunction;
import org.nrg.ecat.edit.Value;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Replace implements ScriptFunction {
    public static final String name = "replace";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List args) throws ScriptEvaluationException {
        final Value original, pre, post;
        try {
            final Iterator i = args.iterator();
            original = (Value)i.next();
            pre = (Value)i.next();
            post = (Value)i.next();
        } catch (NoSuchElementException e) {
            throw new ScriptEvaluationException(name
                    + " takes three arguments [original, pre, post]", e);
        }

        return new Value() {
            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#on(java.util.Map)
             */
            public Object on(final Map m) throws ScriptEvaluationException {
                return original.on(m).toString().replace(pre.on(m).toString(), post.on(m).toString());
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#getVariables()
             */
            public Set getVariables() {
                final Set variables = new LinkedHashSet();
                variables.addAll(original.getVariables());
                variables.addAll(pre.getVariables());
                variables.addAll(post.getVariables());
                return variables;
            }
        };
    }
}
