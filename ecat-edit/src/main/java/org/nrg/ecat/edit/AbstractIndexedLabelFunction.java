/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public abstract class AbstractIndexedLabelFunction implements ScriptFunction {
	protected abstract boolean isAvailable(String label) throws ScriptEvaluationException;
	
	private final Value getFormat(final List values) throws ScriptEvaluationException {
		try {
		return (Value)values.get(0);
		} catch (IndexOutOfBoundsException e) {
			try {
				throw new ScriptEvaluationException(this.getClass().getField("name").get(null)
						+ " requires format argument");
			} catch (IllegalArgumentException e1) {
				throw new RuntimeException(e1);
			} catch (SecurityException e1) {
				throw new RuntimeException(e1);
			} catch (IllegalAccessException e1) {
				throw new RuntimeException(e1);
			} catch (NoSuchFieldException e1) {
				throw new RuntimeException(e1);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.ScriptFunction#apply(java.util.List)
	 */
	public Value apply(final List args) throws ScriptEvaluationException {
		final Value format = getFormat(args);
		return new Value() {
			private NumberFormat buildFormatter(final int len) {
				final StringBuffer sb = new StringBuffer();
				for (int i = 0; i < len; i++) {
					sb.append("0");
				}
				return new DecimalFormat(sb.toString());
			}
			
			private String valueFor(final Object fo) throws ScriptEvaluationException {
				final String format = fo.toString();
				final int offset = format.indexOf('#');
				int end = offset;
				while ('#' == format.charAt(end)) {
					end++;
				}
				final NumberFormat nf = buildFormatter(end - offset);
				final StringBuffer sb = new StringBuffer(format);
				for (int i = 0; true; i++) {
					final String label = sb.replace(offset, end, nf.format(i)).toString();
					if (isAvailable(label)) {
						return label;
					}
				}
			}
			
			public Object on(Map m) throws ScriptEvaluationException {
				return valueFor(format.on(m));
			}
			
			public Set getVariables() {
				return format.getVariables();
			}
		};
	}
}
