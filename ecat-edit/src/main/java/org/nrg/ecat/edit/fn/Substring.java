/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit.fn;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nrg.ecat.edit.IntegerValue;
import org.nrg.ecat.edit.ScriptEvaluationException;
import org.nrg.ecat.edit.ScriptFunction;
import org.nrg.ecat.edit.Value;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class Substring implements ScriptFunction {
	public static final String name = "substring";
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.ScriptFunction#apply(java.util.List)
	 */
	public Value apply(final List args) throws ScriptEvaluationException {
		final Value s = (Value)args.get(0);
		final int start;
		final int end;
		try {
			start = ((IntegerValue)args.get(1)).getValue();
			end = ((IntegerValue)args.get(2)).getValue();
		} catch (ClassCastException e) {
			throw new ScriptEvaluationException("substring[ string, start(integer), end(integer)]");
		}
		return new Value() {
			public Set getVariables() { return s.getVariables(); }
			public Object on(Map m) throws ScriptEvaluationException {
				final Object o = s.on(m);
				return null == o ? null : o.toString().substring(start, end);
			}
		};
	}
}
