/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.util.Map;
import java.util.Set;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface Value {
	Set getVariables();
	Object on(Map m) throws ScriptEvaluationException;
}
