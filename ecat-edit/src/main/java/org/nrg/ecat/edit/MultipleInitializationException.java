/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class MultipleInitializationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public MultipleInitializationException(final Variable v, final Value value) {
		super("Attempted to reinitialize variable " + v + " -> " + value);
	}
}
