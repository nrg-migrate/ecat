/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Variable {
    String getDescription();
    String getExportField();
    String getName();
    Value getInitialValue();
    Object getValue();
    boolean isHidden();
    void setDescription(String description);
    void setExportField(String exportField);
    void setInitialValue(Value value) throws MultipleInitializationException;
    void setIsHidden(boolean isHidden);
    void setValue(Object value);
}
