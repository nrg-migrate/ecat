/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ConstantValue implements Value {
	private final Object value;
	
	public ConstantValue(final Object value) {
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.Value#getVariables()
	 */
	public Set getVariables() { return Collections.EMPTY_SET; }

	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.Value#on(java.util.Map)
	 */
	public Object on(final Map m) { return value; }
}
