/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class VariableValue implements Value {
	private final Variable variable;
	
	public VariableValue(final Variable variable) {
		this.variable = variable;
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.Value#getVariables()
	 */
	public Set getVariables() {
		return Collections.singleton(variable);
	}

	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.Value#on(java.util.Map)
	 */
	public Object on(Map m) throws ScriptEvaluationException {
		final Object v = variable.getValue();
		if (null == v) {
			final Value initial = variable.getInitialValue();
			return null == initial ? null : initial.on(m);
		} else {
			return v;
		}
	}
}
