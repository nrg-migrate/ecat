/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit.fn;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.ecat.edit.IntegerValue;
import org.nrg.ecat.edit.ScriptEvaluationException;
import org.nrg.ecat.edit.ScriptFunction;
import org.nrg.ecat.edit.Value;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Match implements ScriptFunction {
    public static final String name = "match";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(List args) throws ScriptEvaluationException {
        final Value value = (Value)args.get(0);
        final Value pattern = (Value)args.get(1);
        final int group = ((IntegerValue)args.get(2)).getValue();
        return new Value() {
            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#getVariables()
             */
            public Set getVariables() {
                final Set vs = new LinkedHashSet(value.getVariables());
                vs.addAll(pattern.getVariables());
                return vs;
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#on(java.util.Map)
             */
            public Object on(Map m) throws ScriptEvaluationException {
                return evaluate(value.on(m).toString(), pattern.on(m).toString(), group);
            }
        };
    }

    private static String evaluate(final String value, final String pattern, final int group) {
        final Matcher m = Pattern.compile(pattern).matcher(value);
        return m.matches() ? m.group(group) : null;
    }
}
