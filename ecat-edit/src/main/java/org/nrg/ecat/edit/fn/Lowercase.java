/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit.fn;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nrg.ecat.edit.ScriptEvaluationException;
import org.nrg.ecat.edit.ScriptFunction;
import org.nrg.ecat.edit.Value;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class Lowercase implements ScriptFunction {
	public static final String name = "lowercase";
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.ScriptFunction#apply(java.util.List)
	 */
	public Value apply(List args) throws ScriptEvaluationException {
		final Value v;
		try {
			v = (Value)args.get(0);
		} catch (IndexOutOfBoundsException e) {
			throw new ScriptEvaluationException(name + " requires one argument");
		}
		return new Value() {
			public Object on(Map m) throws ScriptEvaluationException {
				return toLower(v.on(m));
			}
			
			public Set getVariables() { return v.getVariables(); }
		};
	}
	
	private static String toLower(final Object o) {
		return null == o ? null : o.toString().toLowerCase();
	}

}
