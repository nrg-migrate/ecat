/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.util.List;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface ScriptFunction {
	Value apply(List args) throws ScriptEvaluationException;
}
