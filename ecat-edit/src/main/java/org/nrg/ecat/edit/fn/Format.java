/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit.fn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.nrg.ecat.edit.MessageFormatValue;
import org.nrg.ecat.edit.ScriptEvaluationException;
import org.nrg.ecat.edit.ScriptFunction;
import org.nrg.ecat.edit.Value;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class Format implements ScriptFunction {
	public static final String name = "format";
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.ScriptFunction#apply(java.util.List)
	 */
	public Value apply(final List args) throws ScriptEvaluationException {
		final Iterator i = args.iterator();
		final Value pattern = (Value)i.next();
		final List elements = new ArrayList(args.size() - 1);
		while (i.hasNext()) {
			elements.add(i.next());
		}
		
		return new MessageFormatValue(pattern, elements);
	}

}
