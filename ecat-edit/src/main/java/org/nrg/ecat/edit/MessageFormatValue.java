/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class MessageFormatValue implements Value {
	private final Value format;
	private final List values;
	private final Set variables;
	
	public MessageFormatValue(final Value format, final Collection values) {
		this.format = format;
		this.values = new ArrayList(values);
		
		// Sort out the dependencies
		final Set tv = new LinkedHashSet();
		for (final Iterator i = this.values.iterator(); i.hasNext(); ) {
			final Value v = (Value)i.next();
			tv.addAll(v.getVariables());
		}
		this.variables = Collections.unmodifiableSet(tv);
	}
	
	public MessageFormatValue(final String format, final Collection values) {
		this(new ConstantValue(format), values);
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.Value#getVariables()
	 */
	public Set getVariables() { return variables; }

	/* (non-Javadoc)
	 * @see org.nrg.ecat.edit.Value#on(java.util.Map)
	 */
	public Object on(Map m) throws ScriptEvaluationException {
		final Object[] vals = new Object[values.size()];
		for (int i = 0; i < values.size(); i++) {
			vals[i] = ((Value)values.get(i)).on(m);
		}
		final Object f = format.on(m);
		return MessageFormat.format(null == f ? "" : f.toString(), vals);
	}
}
