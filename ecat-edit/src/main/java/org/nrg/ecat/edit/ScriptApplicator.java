/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import org.nrg.ecat.edit.fn.Format;
import org.nrg.ecat.edit.fn.Lowercase;
import org.nrg.ecat.edit.fn.Match;
import org.nrg.ecat.edit.fn.Replace;
import org.nrg.ecat.edit.fn.Substring;
import org.nrg.util.GraphUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class ScriptApplicator {
    private final Logger logger = LoggerFactory.getLogger(ScriptApplicator.class);
    private final EditECATASTParser astParser;

    public ScriptApplicator(final InputStream script, final Map functions)
    throws IOException,ScriptEvaluationException {
        if (null == script) {
            astParser = null;
        } else {
            try {
                logger.trace("setting up ECAT edit lexer");
                final EditECATLexer lexer = new EditECATLexer(new ANTLRInputStream(script));
                logger.trace("setting up ECAT edit parser");
                final EditECATParser parser = new EditECATParser(new CommonTokenStream(lexer));
                logger.trace("parsing ECAT edit script to AST");
                final EditECATParser.script_return sr = parser.script();

                final CommonTree ast = (CommonTree)sr.getTree();
                if (null == ast) {
                    logger.trace("unparseable");
                    astParser = null;
                } else {
                    astParser = new EditECATASTParser(new CommonTreeNodeStream(ast));
                    astParser.setFunction(Substring.name, new Substring());
                    astParser.setFunction(Format.name, new Format());
                    astParser.setFunction(Lowercase.name, new Lowercase());
                    astParser.setFunction(Replace.name, new Replace());
                    astParser.setFunction(Match.name, new Match());
                    for (final Iterator mei = functions.entrySet().iterator(); mei.hasNext(); ) {
                        final Map.Entry me = (Map.Entry)mei.next();
                        logger.trace("adding function {}", me);
                        astParser.setFunction((String)me.getKey(), (ScriptFunction)me.getValue());
                    }
                    logger.trace("parsing ECAT edit AST");
                    astParser.script();
                    logger.trace("script-defined variables: {}", astParser.getVariables());
                }
            } catch (RecognitionException e) {
                logger.error("error parsing ECAT script", e);
                throw new ScriptEvaluationException("error parsing script", e);
            }
        }
    }

    public Variable getVariable(final String label) {
        return astParser.getVariable(label);
    }

    public Map getVariables() {
        return null == astParser ? Collections.EMPTY_MAP : astParser.getVariables();
    }

    public List getSortedVariables(final Collection excluding) {
        if (null == astParser) {
            return Collections.EMPTY_LIST;
        }

        final Map graph = new LinkedHashMap();
        for (final Iterator i = astParser.getVariables().values().iterator(); i.hasNext(); ) {
            final Variable v = (Variable)i.next();
            if (!excluding.contains(v)) {
                final Value iv = v.getInitialValue();
                final Collection dependencies = new ArrayList();
                if (null != iv) {
                    for (final Iterator di = iv.getVariables().iterator(); di.hasNext(); ) {
                        final Variable dv = (Variable)di.next();
                        if (!excluding.contains(dv.getName())) {
                            dependencies.add(dv);
                        }
                    }
                }
                graph.put(v, dependencies);
            }
        }
        return GraphUtils.topologicalSort(graph);
    }

    public List getSortedVariables(final String[] excluding) {
        return getSortedVariables(Arrays.asList(excluding));
    }

    public List getSortedVariables() {
        return getSortedVariables(Collections.EMPTY_LIST);
    }
}
