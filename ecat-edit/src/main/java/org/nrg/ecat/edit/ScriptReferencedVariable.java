/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ScriptReferencedVariable implements Variable {
    private final Logger logger = LoggerFactory.getLogger(ScriptReferencedVariable.class);
    private final String name;
    private Object value = null;
    private Value initValue = null;
    private boolean isHidden = false;
    private String description = null;
    private String exportField = null;

    public ScriptReferencedVariable(final String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#getDescription()
     */
    public String getDescription() { return description; }

    /*
     * (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#getExportField()
     */
    public String getExportField() { return exportField; }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#getInitialValue()
     */
    public Value getInitialValue() { return initValue; }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#getName()
     */
    public String getName() { return name; }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#getValue()
     */
    public Object getValue() { return value; }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#isHidden()
     */
    public boolean isHidden() { return isHidden; }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#setDescription(java.lang.String)
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#setExportField(java.lang.String)
     */
    public void setExportField(final String exportField) {
        this.exportField = exportField;
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#setInitialValue(org.nrg.ecat.edit.Value)
     */
    public void setInitialValue(final Value value) throws MultipleInitializationException{
        if (null == initValue) {
            logger.trace("{} initial value set to {}", this, value);
            initValue = value;
        } else {
            throw new MultipleInitializationException(this, value);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#setIsHidden(boolean)
     */
    public void setIsHidden(final boolean isHidden) {
        this.isHidden = isHidden;
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.Variable#setValue(java.lang.Object)
     */
    public void setValue(final Object value) {
        logger.trace("{} value set to {}", this, value);
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuffer sb = new StringBuffer(super.toString());
        return sb.append(" (").append(name).append(")").toString();
    }
}
