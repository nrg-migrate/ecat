tree grammar EditECATASTParser;

options {
	tokenVocab = EditECATParser;
	ASTLabelType = CommonTree;
}

@header {
	package org.nrg.ecat.edit;
	
	import java.io.IOException;
	import java.util.Collections;
	import java.util.Map;
	import java.util.HashMap;
	import java.util.TreeMap;
}

@members {
	private final Map variables = new TreeMap();
	private final Map functions = new HashMap();
	
  /**
   * Adapted from JSON interpreter string extractor by Richard Clark
   **/
  private static String extractString(final CommonTree token) {
    final StringBuffer sb = new StringBuffer(token.getText());
    
    sb.deleteCharAt(0); // remove leading and trailing quotes
    sb.deleteCharAt(sb.length() - 1);
    
    int i = 0;
    while (i < sb.length() - 1) {
      final int iesc = sb.indexOf("\\", i);  // look for a backslash
      if (-1 == iesc) {
        break;  // no backlashes found; move on
      }
      
      final int cesc = sb.charAt(iesc + 1);
      switch (cesc) {
        case 'b': // backspace
          sb.replace(iesc, iesc + 2, "\b");
          break;
          
        case 't': // tab
          sb.replace(iesc, iesc + 2, "\t");
          break;
          
        case 'n': // newline
          sb.replace(iesc, iesc + 2, "\n");
          break;
          
        case 'f': // form feed
          sb.replace(iesc, iesc + 2, "\f");
          break;
          
        case 'r': // return
          sb.replace(iesc, iesc + 2, "\r");
          break;
          
        case '"': // double quote
          sb.replace(iesc, iesc + 2, "\"");
          break;
          
        case '\'':  // single quote
          sb.replace(iesc, iesc + 2, "'");
          break;
          
        case '\\':  // backslash
          sb.replace(iesc, iesc + 2, "\\");
          break;
          
        case 'u': // unicode escape
          throw new UnsupportedOperationException();
          
        case '0': // octal escape
          throw new UnsupportedOperationException();
        
        default:
          throw new UnsupportedOperationException("no support for escape code \\" + cesc);
       }
       i = iesc + 1;
    }
    
    return sb.toString();
  }
	
	private Variable getVariable(final CommonTree token) {
		return getVariable(token.getText());
	}
		
	public Variable getVariable(final String label) {
		final String canonical = label.toLowerCase();
		if (variables.containsKey(canonical)) {
			return (Variable)variables.get(canonical);
		} else {
			final Variable v = new ScriptReferencedVariable(label);
			variables.put(canonical, v);
			return v;
		}
	}
	
	public final Map getVariables() {
		return Collections.unmodifiableMap(variables);
	}
	
	public void setFunction(final String label, final ScriptFunction f) {
		functions.put(label, f);
	}
		
	private Value apply(final String name, final List termlist)
	throws ScriptEvaluationException {
		final ScriptFunction f = (ScriptFunction)functions.get(name);
		if (null == f) {
			throw new ScriptEvaluationException("undefined function " + name);
		} else {
			return f.apply(termlist);
    		}
	}
}

script
	: statement*
	;

statement
	:	action
	|	description
	|	initialization
	;
	
action 
	:	echo
	;

initialization
	:	^(INITIALIZE ID value) {
			try {
				getVariable($ID).setInitialValue($value.v);
			} catch (MultipleInitializationException e) {
				e.printStackTrace();
				throw new RecognitionException();	// TODO: better
			}
		}
	;
	
description
	:	^(DESCRIBE id=ID desc=STRING) {
			getVariable(id).setDescription(extractString(desc));
		}
	|	^(HIDDEN id=ID) {
			getVariable(id).setIsHidden(true);
		}
	|	^(EXPORT id=ID field=STRING) {
			getVariable(id).setExportField(extractString(field));
		}
	;
	
echo
	:	^(ECHO v=value) {
			try {
            			System.out.println(v.on(Collections.EMPTY_MAP));
			} catch (ScriptEvaluationException e) {
				e.printStackTrace();
			}
		}
	|	ECHO {
           		System.out.println();
		};

term returns [Value v]
	:	STRING	{ $v = new ConstantValue(extractString($STRING)); }
	|	NUMBER	{ $v = new IntegerValue($NUMBER.getText()); }
	|	ID		{ $v = new VariableValue(getVariable($ID)); }
	|	^(FUNCTION ID termlist)	{
	   		try {
	   			$v = apply($ID.getText(), $termlist.tl);
	   		} catch (ScriptEvaluationException e) {
	   			e.printStackTrace();
	   			throw new RecognitionException(input);	// TODO: better
	   		}
	   	}
	;
	
termlist returns [List tl]
	@init {
		tl = new ArrayList();
	}
	:	(t=term { tl.add(t); })*
	;
	
value returns [Value v]
 	:	t=term { $v = t; }
	;
