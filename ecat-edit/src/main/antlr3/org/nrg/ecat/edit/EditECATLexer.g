lexer grammar EditECATLexer;

@header {
	package org.nrg.ecat.edit;
}

LEFT	:	'[';
RIGHT	:	']';
COMMA	:	',';

CONSTRAINS :	':';
DELETE	:	'-';
EQUALS	:	'=';
MATCHES	:	'~';
ASSIGN	:	':=';

ECHO	:	'echo';
NEW	:	'new';
DESCRIBE:	'describe';
HIDDEN	:	'hidden';
EXPORT  :	'export';

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | ('\\' '\r'? '\n')
        ) {$channel=HIDDEN;}
    ;
    
NEWLINE	:	'\r'? '\n';



STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

NUMBER	:	'0' | ('-'? '1'..'9' DIGIT*);

fragment LPAREN	:	'(';

fragment RPAREN	:	')';

fragment DIGIT	:	('0'..'9');

fragment HEXDIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT
    ;
