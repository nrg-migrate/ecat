/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.ecat.edit;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.Map;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ScriptApplicatorTest extends TestCase {
    private static final Map fns = Collections.emptyMap();
    
    private static final String S_DESCRIPTION = "describe foo \"Description\"\nbar := foo\n";
    
    private static final String S_EXPORT_FIELD = "export foo \"baz:/my/export/path\"\nbar := foo\n";

    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }
    

    public void testDescription() throws Exception {
        final ScriptApplicator applicator = new ScriptApplicator(bytes(S_DESCRIPTION), fns);
        final Variable foo = applicator.getVariable("foo");
        assertEquals("Description", foo.getDescription());
        final Variable bar = applicator.getVariable("bar");
        assertNull(bar.getDescription());
    }
    
    public void testExportField() throws Exception {
        final ScriptApplicator applicator = new ScriptApplicator(bytes(S_EXPORT_FIELD), fns);
        final Variable foo = applicator.getVariable("foo");
        assertEquals("baz:/my/export/path", foo.getExportField());
        final Variable bar = applicator.getVariable("bar");
        assertNull(bar.getExportField());   
    }
}
